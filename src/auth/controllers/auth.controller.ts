import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { LocalAuthGuard } from '../guards/local-auth.guard';
import { AuthService } from '../services/auth.service';

@UseGuards(JwtAuthGuard)
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }
    // @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Body() req:any) {
        return this.authService.login(req);
    }

    
    @Get('profile')
    getProfile(@Request() req) {
        return req.user;
    }

}
