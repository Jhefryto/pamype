import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../../general/users/service/users.service';
import * as bcryptjs from 'bcryptjs'

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findUser(username);
    if (user) {
      const isMatch = await bcryptjs.compare(pass, user.password);
      if (isMatch) {
        return user;
      } else {
        throw new UnauthorizedException(`Contraseña Incorrecto`)
      }
    }
    return false;
  }

  async login(user: any) {
    const validar = await this.validateUser(user.username, user.password)
    if (!validar) {
      throw new UnauthorizedException(`Usuario y/o Contrañeña Incorrecto`)
    }else if (validar.estado === 0){
      throw new UnauthorizedException(`Usuario Inactivo, consulte con el Administrador`)
    }
    const payload = { username: validar.userName, sub: validar.id };
    const { id, password, ...result } = validar;
    return {
      access_token: this.jwtService.sign(payload),
      result
    };
  }
}
