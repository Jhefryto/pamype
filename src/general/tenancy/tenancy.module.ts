import {
  BadRequestException,
  forwardRef,
  MiddlewareConsumer,
  Module,
  RequestMethod,
} from '@nestjs/common';
import { TenancyService } from './tenancy.service';
import { TenancyController } from './tenancy.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tenancy } from './tenancy.entity';
import { TenantProvider } from './tenancy.provider';
import { LocalStrategy } from './../../auth/strategy/local.strategy';
import { AuthModule } from 'src/auth/auth.module';
import { LoggerMiddleware } from './tenancy.middleware';
import { AuthService } from 'src/auth/services/auth.service';
import { JwtStrategy } from 'src/auth/strategy/jwt.strategy';

@Module({
  imports: [forwardRef(() => AuthModule), TypeOrmModule.forFeature([Tenancy])],
  providers: [TenancyService, TenantProvider],
  exports: [TenantProvider],
  controllers: [TenancyController],
})
export class TenancyModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .exclude({ path: '/api/tenants', method: RequestMethod.ALL })
      .forRoutes('*');
  }
}
