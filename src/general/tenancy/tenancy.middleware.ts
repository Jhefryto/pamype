import { BadRequestException, Injectable, NestMiddleware } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request, Response, NextFunction } from 'express';
import { Connection, createConnection, getConnection } from 'typeorm';
import { Role } from '../users/entities/role.entity';
import { User } from '../users/entities/user.entity';
import { Tenancy } from './tenancy.entity';
import { TenancyService } from './tenancy.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    constructor(
        private readonly connection: Connection,
        private readonly configService: ConfigService,
        private readonly tenantService: TenancyService,
    ) { }
    async use(req: Request, res: Response, next: NextFunction) {
        const arrayParam: string = req.params[0];
        const name = arrayParam.split('/');
        const tenant: Tenancy = await this.tenantService.findOne(name[0]);
        // console.log(name)

        if (!tenant) {
            throw new BadRequestException(
                'Database Connection Error',
                'This tenant does not exists',
            );
        }

        let dataBase = tenant.name;
        (dataBase === 'general') ? dataBase = 'DBGENERAL' : '';
        try {
            getConnection(dataBase);
            next();
        } catch (e) {
            const result = await this.connection.query(
                `SELECT datname FROM pg_database WHERE datname = '${dataBase}'`,
            );

            // console.log(result)
            if (!result[0]) {
                await this.connection.query(
                    `CREATE DATABASE ${dataBase}`,
                );
            }

            const createdConnection: Connection = await createConnection({
                name: dataBase,
                type: 'postgres',
                host: this.configService.get('DB_HOST'),
                port: +this.configService.get('DB_PORT'),
                username: this.configService.get('DB_USER'),
                password: this.configService.get('DB_PASSWORD'),
                database: dataBase,
                entities: [User,Role],
                // ssl: true,
                synchronize: true,
            });

            if (createdConnection) {
                // console.log(createdConnection)
                next();
            } else {
                throw new BadRequestException(
                    'Database Connection Error',
                    'There is a Error with the Database!',
                );
            };
        };
    };
}
