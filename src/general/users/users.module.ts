import { Module } from '@nestjs/common';
import { UsersService } from './service/users.service';
import { UsersController } from './controllers/users.controller';
import { TenancyModule } from '../tenancy/tenancy.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { RoleController } from './controllers/role.controller';
import { RoleService } from './service/role.service';
import { Role } from './entities/role.entity';

@Module({
  imports: [TenancyModule],
  providers: [UsersService, RoleService],
  exports: [UsersService],
  controllers: [UsersController, RoleController],
})
export class UsersModule { }
