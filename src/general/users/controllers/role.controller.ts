import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../../../auth/guards/jwt-auth.guard';
import { CreateRoleDto } from '../dto/create-role.dtos';
import { RoleService } from '../service/role.service';

@UseGuards(JwtAuthGuard)
@Controller('role')
export class RoleController {
    constructor(private readonly roleService: RoleService) { }

    @Get()
    findAll() {
        return this.roleService.findAll();
    }
    
    @Post()
    async create(@Body() user: CreateRoleDto) {
        return this.roleService.create(user);
    }
}
