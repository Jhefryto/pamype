import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';

import { JwtAuthGuard } from './../../../auth/guards/jwt-auth.guard';
import { UsersService } from '../service/users.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';
import { UpdateUserDto } from '../dto/update-user.dto';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Get()
    findAll() {
        return this.usersService.findAll();
    }

    @Get('buscar')
    findUser(@Query('userName') userName: string,) {
        return this.usersService.findUser(userName);
    }

    @Post()
    async create(@Body() user: CreateUserDto) {
      return this.usersService.create(user);
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() data?: UpdateUserDto) {
      return this.usersService.update(id, data);
    }

    @Delete(':id')
    delete(@Param('id') id: number) {
      return this.usersService.remove(id);
    }
}
