import { Inject, Injectable, NotAcceptableException, Scope } from '@nestjs/common';
import { TENANT_CONNECTION } from './../../../general/tenancy/tenancy.provider';
import { Connection, Repository } from 'typeorm';
import { Role } from '../entities/role.entity';
import { plainToClass } from 'class-transformer';
import { CreateRoleDto } from '../dto/create-role.dtos';

@Injectable({ scope: Scope.REQUEST })
export class RoleService {
    private readonly roleRepository: Repository<Role>;

    constructor(@Inject(TENANT_CONNECTION) connection: Connection) {
        this.roleRepository = connection.getRepository(Role);
    }

    async findAll() {
        const roles = await this.roleRepository.find();
        return roles.map(role => plainToClass(Role, role));
    }

    async findUser(name: string) {
        return await this.roleRepository.findOne({name});
    }

    async create(role: CreateRoleDto): Promise<Role> {
        const userFind = await this.findUser(role.name);
        if (userFind) {
            throw new NotAcceptableException(`El nombre del Rol ya existe actulice la lista o intente con otro nombre`)
        }
        const newRole = this.roleRepository.create(role);
        return this.roleRepository.save(newRole);

    }
}
