
import { Inject, Injectable, NotAcceptableException, NotFoundException, Scope } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { Connection, Repository } from 'typeorm';
import * as bcryptjs from 'bcryptjs'
import { TENANT_CONNECTION } from './../../../general/tenancy/tenancy.provider';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { Role } from '../entities/role.entity';

@Injectable({ scope: Scope.REQUEST })
export class UsersService {

  private readonly userRepository: Repository<User>;
  private readonly roleRepository: Repository<Role>;

  constructor(@Inject(TENANT_CONNECTION) connection: Connection) {
    this.userRepository = connection.getRepository(User);
    this.roleRepository = connection.getRepository(Role);
  }

  async findAll() {
    const users = await this.userRepository.find();
    return users.map(user => plainToClass(User, user));
  }

  async findUser(userName: string) {
    return await this.userRepository.findOne({ userName });
  }

  async create(user: CreateUserDto): Promise<User> {
    const userFind = await this.findUser(user.userName);

    if (userFind) {
      throw new NotAcceptableException(`El ususario ya existe, actulice la lista o intente con otro nombre`)
    }

    const newUser = this.userRepository.create(user);
    if (user.id_role) {
      newUser.role = await this.validateRole(user.id_role);
    }
    const hashPassword = await bcryptjs.hash(newUser.password, 10);
    newUser.password = hashPassword;

    return this.userRepository.save(newUser);

  }

  async update(id: number, data: UpdateUserDto) {
    const user = await this.userRepository.findOne(id)
    if (!user) {
      throw new NotFoundException(`Usuario no existe actulice la lista y/o consulte su proveedor`);
    }
    if (data.id_role) {
      user.role = await this.validateRole(data.id_role);
    }
    const updateUser = this.userRepository.merge(user, data);
    const hashPassword = await bcryptjs.hash(updateUser.password, 10);
    updateUser.password = hashPassword;
    return this.userRepository.save(user);
  }

  async remove(id: number) {
    const user = await this.userRepository.findOne(id);
    if (user && user.estado != 1) {
      throw new NotAcceptableException(`El usuario ya es inactivo, porfavor actialice la lista `);
    } else if (user) {
      const data = {
        'estado': 0
      };
      this.userRepository.merge(user, data);
      return this.userRepository.save(user);
    }
  }

  async validateRole(id: number) {
    const rol = await this.roleRepository.findOne(id);
    if (!rol) {
      throw new NotAcceptableException(`El rol no existe`)
    }
    return rol;
  }
}