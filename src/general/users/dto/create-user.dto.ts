import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsPositive, IsString, IsUrl } from 'class-validator';

@Exclude()
export class CreateUserDto {
  @IsString()
  @Expose()
  @IsNotEmpty()
  readonly name: string;

  @IsString()
  @Expose()
  @IsNotEmpty()
  readonly userName: string;

  @IsString()
  @Expose()
  @IsNotEmpty()
  readonly password: string;

  @IsNotEmpty()
  @IsUrl()
  @Expose()
  readonly image: string;

  @IsPositive()
  @Expose()
  @IsNotEmpty()
  readonly id_role: number;
}
